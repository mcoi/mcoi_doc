\documentclass{lhcreport}
\usepackage{graphicx}
\usepackage{float}
\usepackage{multirow}
\usepackage{adjustbox}
\usepackage{colortbl}
\usepackage{multicol}
\usepackage{hyperref}
\usepackage{colortbl}
\usepackage[printonlyused]{acronym}
\usepackage{listings}
\usepackage{tikz}
\usetikzlibrary{shapes,arrows}

% define colors of cells
\definecolor{cellblue}{rgb}{0.823,0.894,0.9882}
\definecolor{cellred}{rgb}{0.9882,0.8196,0.945}
\definecolor{cellgreen}{rgb}{0.8196,0.9882,0.9777}
\definecolor{Brown}{cmyk}{0,0.81,1,0.60}
\definecolor{OliveGreen}{cmyk}{0.64,0,0.95,0.40}
\definecolor{CadetBlue}{cmyk}{0.62,0.57,0.23,0}


\docnumber{}
\cernsec{BE-BI-PI}
\edms{DRAFT}
\keywords{LHC, MIDI}
\doctype{Development Documentation}
\title{Motor Controller with Optical Interface}
\author{Felix Arnold BE-BI-PM
  David Belohrad BE-BI-PI}
\date{October 2017}

\abstract{This document discusses the technical implementation of the
  Motor Controller with Optical Interface (MCOI). The MCOI consists of a back-end unit and a front-end unit which are connected via an optical link. This configuration shall facilitate the deployment of the motors and controllers for different applications managed by the CERN BE-BI-PM section. This project based on the HI REX upgrade for ISOLDE which served a similar purpose but was not equipped with an optical link but with a parallel electrical interface instead.}

\email{david.belohrad@cern.ch}

\approvalmembers{}
%\checkedby{Lars Jensen}
\approvalleader{}

\begin{document}
\maketitle
\tableofcontents

\chapter{INTRODUCTION}
The Motor Controller with Optical Interface (MCOI) consists of two
units connected through an optical link:
\begin{itemize}
\item a back-end unit, which is a standard VME crate equipped with VFC
  cards. Each VFC card can provide up to 4 GBTx links to the
  front-ends
\item multiple front-ends, a Europa-crate style units containing one
  GEFE module interfacing up to 16 stepper motors (motor crate, MC)
\end{itemize}

This configuration facilitates the deployment of the motors and controllers for different applications managed by the CERN BE-BI-PM section. The project based on the HI REX upgrade for ISOLDE which served a similar purpose but was not equipped with an optical link but with a parallel electrical interface instead.

\chapter{OVERVIEW}
\begin{figure}[H]
  \includegraphics[width=\linewidth]{./pictures/Overview.pdf}
  \caption{MCOI project block schematic}

\end{figure}



\chapter{BACK-END - VFC-HD}

The MCOI Back-end consists of.
\begin{itemize}
\item Standard VME Crate including
  \begin{itemize}
  \item backplane
  \item CPU card with Ethernet interface
  \item power supply unit
  \end{itemize}
\item VFC-HD carriers
\end{itemize}

The MCOI Back-end has the following interfaces:
\begin{itemize}
\item The CPU acts as a master on the VME bus and connects the crate the the outside via an Ethernet interface
\item Each VFC implements VME bus slave and acts as a command and
  control unit for the front-ends.
\item The commands to the Motor Crates are sent via up to 4 optical
  links per VFC, using SFP interfaces of the VFC.
\end{itemize}

The MCOI Back-end, the Motor Controller, has the following main functionality:
\begin{itemize}
\item Execution of elementary motor commands, e.g. steps, direction, speed
\item Possibility to generate an interrupt when the command has been executed
\item Using the limit switch status as a feedback to stop at the end-positions of the motor
\item Possibility to execute several commands in a series by implementing a command queue
\end{itemize}

\section{FPGA}
The FPGA used in the VFC-HD is the 300K logic elements version of Arria V GX from Altera/Intel.

\subsection{Block Diagram}
The block diagram of the FPGA is shown in Figure 2.
\begin{figure}[H]
  \includegraphics[width=\linewidth]{./pictures/FPGA.pdf}
  \caption{MCOI FPGA Block Diagram}
\end{figure}

The VFC FPGA code consists of the VME to Wishbone interface. All the
FPGA functionality in terms of internal modules communication is realised using a
Wishbone Crossbar. The crossbar is connected to an Interrupt
Controller (IC) slave, and multiple Wishbone slaves (WS). Up to 16
wishbone slaves (chosen at the time of code compilation)
implement logic behind the motor control, one wishbone slave
implements global registry, and implements functionality common to all
motors, as e.g. serial number of the VFC and firmware revision of the
code.

\subsection{Address Map}
The VFC base address is derived from the hardware switches as seen in
Tab.~\ref{tab:baseaddr}. The hardware switch M3 identifies, whether
the VFC uses geographic addressing (pins GA0-GA4 and GAP of the VME64x
bus), or is specified by address switches BOARD AD[3:0].

The VME address map is listed in Tab.~\ref{tab:registers}.

\begin{table}[b]
  \centering
  \begin{tabular}{|l|p{7cm}|}
\hline
Base Address & Switch M3 on VME card:\\
& 0 Local Addressing \\
& 1 Geographical Addressing \\
\hline
Local Address & Switches BOARD AD[3:0]:
4 bits -\textgreater 16 addresses \\
\hline
  \end{tabular}
  \caption{Base address selector of the VFC VME controller}
  \label{tab:baseaddr}
\end{table}


\begin{table}
  \tiny
  \centering
  \caption{Address map}
  \begin{adjustbox}{max width=\textwidth}
    \begin{tabular}{|c|c|c|c|c|c|c|c|}
      \hline
      \multicolumn{8}{|l|}{Global registers} \\ \hline
      \multirow{2}{*}{Name} & \multicolumn{2}{l|}{Address offset}  & \multirow{2}{*}{Width} & \multirow{2}{*}{Description} & \multirow{2}{*}{Mode} & \multirow{2}{*}{Length} & \multirow{2}{*}{Values} \\
      \cline{2-3} & Channel1 {[}17:14{]} & Reg{[}13:2{]} &&&&& \\ \hline
      ID & 0x0000 & 0x0000 & 32 & Identifier & R & 32 & 0xabb1234 \\ \hline
      FirmWare Version & 0x0000 & 0x0000 & 32 & Firmware versoion (Ex. 0xBB000603 & R & 32 & \begin{tabular}[c]{@{}l@{}}B3=0xBB\\ B2=0x00\\ B1= Version {[}1:0{]}\\ B0= SubVersion {[}1:0{]}\end{tabular} \\ \hline
      Global Reset & 0x0000 & 0x0002 & 32 & Command for global reset (in parallel with VME & W & 0 & Don't care \\ \hline
      Trig & 0x0000 & 0x0003 & 32 & Software trigger, can be used to start the motor movement and/or the ADC acquisition & W & 0 & Don't care \\ \hline
      \multicolumn{8}{|c|}{Motor Command Parameters} \\ \hline
      Steps to Move & 0x0001 - 0x1000 & 0x1000 & 32 & Number of steps for next move cicle & R/W & 32 & 0 - 0xFFFFFFFF \\ \hline
      Movement Direction & 0x0001 - 0x1000 & 0x2101 & 32 & Movement direction & R/W & 1 & \begin{tabular}[c]{@{}l@{}}0 == Positive\\ 1 == Negative\end{tabular} \\ \hline
      Start Speed & 0x0001 - 0x1000 & 0x2102 & 32 & Low speed register reg = (step/s) *  0.262 & R/W & 17 & \begin{tabular}[c]{@{}l@{}}0- 0x1FFFF\\ 0 - 50k stp/s\end{tabular} \\ \hline
      Cruise Speed & 0x0001 - 0x1000 & 0x2103 & 32 & High speed register reg = (step/s) *  0.262 & R/W & 17 & \begin{tabular}[c]{@{}l@{}}0 - 0x1FFFF\\ 0 - 50k stp/s\end{tabular} \\ \hline
      Acceleration & 0x0001 - 0x1000 & 0x2104 & 32 & Acceleration/Deceleration register ref = (steps/s\textasciicircum 2) * 0.0687 & R/W & 19 & \begin{tabular}[c]{@{}l@{}}0 - 0x7FFFF\\ 0 - 7M stp/s\textasciicircum 2\end{tabular} \\ \hline
      Trail & 0x0001 - 0x1000 & 0x2105 & 32 & Sets the number of steps at low speed to be used at the end of the movement circle & R/W & 32 & 0 - 0xFFFFFFFF \\ \hline
      Trigger Enable & 0x0001 - 0x1000 & 0x2106 & 32 & Enables the internal and/or external triggers to stars a movement & R/W & 2 & \begin{tabular}[c]{@{}l@{}}Bits (1 == enabled) : \\ 0 External trigger\\ 1 Internal trigger\end{tabular} \\ \hline
      GenInterupt & 0x0001 - 0x1000 & 0x2106 & 32 & Generates an interupt at the end of movement & R/W & 2 & 2: Generate Interrupt \\ \hline
      Power Config & 0x001 - 0x1000 & 0x2107 & 32 & Power parameters & RW & 3 & \\ \hline
      \multicolumn{1}{|l|}{End Switch Config} & \multicolumn{1}{l|}{0x0001 - 0x1000} & \multicolumn{1}{l|}{0x2108} & \multicolumn{1}{l|}{32} & \multicolumn{1}{l|}{End Switch configuration} & \multicolumn{1}{l|}{RW} & \multicolumn{1}{l|}{6} & \multicolumn{1}{l|}{\begin{tabular}[c]{@{}l@{}}Bits :\\ 0: SW Out generates ESW+\\ 1: SW In generates ESW+\\ 2: ESW+ Active low\\ 3: SW Out generates ESW-\\ 4: SW In generates ESW-\\ 5: ESW- Active low\end{tabular}} \\ \hline
      \multicolumn{8}{|c|}{Motor Control} \\ \hline
      \multicolumn{1}{|l|}{Start Move} & \multicolumn{1}{l|}{0x0001 - 0x1000} & \multicolumn{1}{l|}{0x2200} & \multicolumn{1}{l|}{32} & \multicolumn{1}{l|}{Starts a stepping movement cycle} & \multicolumn{1}{l|}{W} & \multicolumn{1}{l|}{0} & \multicolumn{1}{l|}{Don't care} \\ \hline
      \multicolumn{1}{|l|}{Stop Move} & \multicolumn{1}{l|}{0x0001 - 0x1000} & \multicolumn{1}{l|}{0x2201} & \multicolumn{1}{l|}{32} & \multicolumn{1}{l|}{Stops a stepping movement (if moving, nothing otherwise)} & \multicolumn{1}{l|}{W} & \multicolumn{1}{l|}{0} & \multicolumn{1}{l|}{Don't  care}\\ \hline
       \multicolumn{1}{|l|}{Reset Position Counter} & \multicolumn{1}{l|}{0x0001 - 0x1000} & \multicolumn{1}{l|}{0x2202} & \multicolumn{1}{l|}{32} & \multicolumn{1}{l|}{Resets the position counter} & \multicolumn{1}{l|}{W} & \multicolumn{1}{l|}{0} & \multicolumn{1}{l|}{Don't care} \\ \hline
       \multicolumn{1}{|l|}{DoQueue} & \multicolumn{1}{l|}{0x0001 - 0x1000} & \multicolumn{1}{l|}{0x2203} & \multicolumn{1}{l|}{32} & \multicolumn{1}{l|}{Queue the stepping movement} & \multicolumn{1}{l|}{W} & \multicolumn{1}{l|}{0} & \multicolumn{1}{l|}{Don't care} \\ \hline
       \multicolumn{8}{|c|}{Motor Status} \\ \hline
       \multicolumn{1}{|l|}{Position counter} & \multicolumn{1}{l|}{0x0001 - 0x1000} & \multicolumn{1}{l|}{0x2000} & \multicolumn{1}{l|}{32} & \multicolumn{1}{l|}{Current position counter (updated during movements)} & \multicolumn{1}{l|}{R} & \multicolumn{1}{l|}{32} & \multicolumn{1}{l|}{0 - 0xFFFFFFFF} \\ \hline
       \multicolumn{1}{|l|}{Error} & \multicolumn{1}{l|}{0x0001 - 0x1000} & \multicolumn{1}{l|}{0x2001} & \multicolumn{1}{l|}{32} & \multicolumn{1}{l|}{} & \multicolumn{1}{l|}{R} & \multicolumn{1}{l|}{32} & \multicolumn{1}{l|}{0 - 0xFFFFFFFF} \\ \hline
        \multicolumn{1}{|l|}{End Switch Status} & \multicolumn{1}{l|}{0x0001 - 0x1000} & \multicolumn{1}{l|}{0x2001} & \multicolumn{1}{l|}{32} & \multicolumn{1}{l|}{} & \multicolumn{1}{l|}{} & \multicolumn{1}{l|}{} & \multicolumn{1}{l|}{TBD} \\ \hline
        \multicolumn{1}{|l|}{Command Queue Status} & \multicolumn{1}{l|}{} & \multicolumn{1}{l|}{} & \multicolumn{1}{l|}{} & \multicolumn{1}{l|}{} & \multicolumn{1}{l|}{} & \multicolumn{1}{l|}{} & \multicolumn{1}{l|}{} \\ \hline
        \multicolumn{1}{|l|}{Command Queue Reset} & \multicolumn{1}{l|}{} & \multicolumn{1}{l|}{} & \multicolumn{1}{l|}{} & \multicolumn{1}{l|}{} & \multicolumn{1}{l|}{} & \multicolumn{1}{l|}{}  & \multicolumn{1}{l|}{} \\ \hline
    \end{tabular}
  \end{adjustbox}
  \label{tab:registers}
\end{table}

\section{Typical Operational Flow}

A typical flow for a single command is the following:
\begin{itemize}
\item The VME master sets the motor command parameters (See Tab.~\ref{tab:registers})
\item The VME master sends the Start Move command or an internal/external trigger pulse is generated.
\end{itemize}


Queued commands permit to describe the entire sequence of
movement. The movement typically consists of acceleration, cruising,
deceleration and final trailing. A typical flow for queued commands is the following:
\begin{itemize}
\item The VME master sets the motor command parameters for the first movement (See Tab.~\ref{tab:registers})
\item The VME master sends the Do Queue command
\item The VME master sets the motor command parameters for the second movement (See Tab.~\ref{tab:registers})
\item The VME master sends the Do Queue command
\item Start Move command OR an internal/external trigger pulse is generated.
\end{itemize}


\chapter{FRONT-END}
\section{Setup}
The MC front-end consists of the following parts:
\begin{itemize}
\item \href{https://www.ohwr.org/projects/gefe/wiki/wiki}{GBT-based Expandable Front-End}
\item \href{https://edms.cern.ch/ui/#!master/navigator/item?P:1830333315:1500236972:subDocs}{Motor Driver Interface (MDI) Board}
\item \href{https://edms.cern.ch/ui/#!master/navigator/project?P:1201687617:1201687617:subDocs}{Motor Driver Backplane (MDB) Board}
\item Power supply unit (PSU)
\item \href{ftp://ftp.phytron.de/manuals/power_stages/zmxplus-en.pdf}{Stepper Motor drivers}
\item Stepper Motors
\item Limit switches
\end{itemize}

\begin{figure}[H]
  \includegraphics[width=\linewidth]{./pictures/Front-end_Overview.pdf}
  \caption{Front-End Overview}
\end{figure}

The front-end is equipped with following interfaces:
\begin{itemize}
\item Optical link to the back-end. The transceiver is not implemented
  as a part of the FPGA design, but it is implemented in a separate
  radiation-tolerant
  \href{https://cms-docdb.cern.ch/cgi-bin/PublicDocDB/ShowDocument?docid=4802}{VTRX}
  ASIC used in the CERN experiments
\item FMC HPC connector between GEFE and MDI
\item A DIN 41612 connector with 160 poles between MDI and MDB
\item Interfaces to the motor drivers, motors and switches on the MDB
\end{itemize}
 The front-end provides the following functionality:
 \begin{itemize}
\item De-serialization of the data received via optical interface from the back-end
\item Serialization of the feedback and status interface to be sent to the back-end
\item Signal conditioning of the digital control and status signals
  \begin{itemize}
   \item Generation of 5V TTL control signals for the motor driver interface
\item Level-shift the 5V TTL status signals from the motor driver interface to a 3.3V level
\item Filter and level-shift the signals from the limit switches
  \end{itemize}
 \item  Power conditioning, e.g. 12V for limit switches
 \end{itemize}

 The communication between the VFC and MC is realised using signals
 shown in Fig.~\ref{fig:controlsignals}. Each motor unit has a separate
 communication link. All links are serialised into the stream and
 transported through the optical layer.

  \begin{figure}[H]
 \begin{centering}
  \includegraphics[width=0.7\linewidth]{./pictures/FE-BE.pdf}
  \caption{FE/BE status and control signals}
  \label{fig:controlsignals}
 \end{centering}
\end{figure}


 \section{Mechanical design}

The mechanical design including dimensions is shown in
Fig.~\ref{fig:mechanics}. A standard Europa 3U crate with a
rear extension is used. A backplane is installed in the middle of the
crate. The 16 motor driver modules are installed into the back-plane
(MDB) from the front. GEFE is installed from the rear side. The
optical interface is available at the front of the GEFE and therefore
it is accessible to the operator. The MDI is attached to the GEFE
through the FMC
connector and interfaces the motor drivers via the 160-pin DIN
connector, which plugs into the backplane
(MDB). Fig.~\ref{fig:mechanics} does not show the PSU. The PSU is
mounted to the crate such that the main power line is accessible from
the front.

\begin{figure}[H]
  \begin{centering}
  \includegraphics[width=\linewidth]{./pictures/RearView.pdf}
  \caption{Front-end mechanical design viewed from the rear}
  \label{fig:mechanics}
  \end{centering}
\end{figure}

\section{FPGA}
The FPGA on the GEFE is the ProAsic3 A3P1000 featuring 24k VersaTiles.

\section{Motor Driver}
The
\href{http://www.phytron-elektronik.de/antrieb/index.php?Set_ID=160&PID=43}{ZMX+}
module from Phytron is used. It is a general purpose stepping driver
controller for 3U 19'' racks.

\section{Power Supply Unit}
The power supply unit (PSU) of the front-end crate consists of:
\begin{itemize}
\item Power Entry module including filter and fuse protection. The
  module is the IEC Inet Filters
  \href{https://www.schaffner.com/product-storage/datasheets/fn-390}{FN390}
  from Schaffner.
\item
  \href{https://www.tracopower.com/products/browse-by-category/find/tsp/3/}{TRACO
    TSP 180-148} industrial power supply, generating 56V from the
  220V AC to transform the 220V.
\end{itemize}

\section{Motor Driver Interface (MDI)}

\begin{table}[]
\centering
\caption{MDI Specification}
\label{tab:mdi}
\begin{tabular}{|l|l|}
\hline
Interface to Carrier   & FMC HPC male                                   \\ \hline
Interface to Backplane & DIN 41612 connector male with 160 poles, 90deg \\ \hline
5V TTL drivers         & 16 x 4 = 64                                    \\ \hline
5V TTL receivers       & 16                                             \\ \hline
Switch interfaces      & 16 x 2 = 32                                    \\ \hline
\end{tabular}
\end{table}


\section{Motor Driver Backplane}

\begin{table}[]
\centering
\caption{MDB Specification}
\label{tab:mdb}
\begin{tabular}{|l|l|}
\hline
Interface to Mezzanine   & DIN 41612 connector female with 160 poles  \\ \hline
Phytron ZMX+ IF          & 16 x 48 pin VG connector DIN 41612, type F \\ \hline
Motor + Switch Interface & 16 x custom IF                            \\ \hline
\end{tabular}
\end{table}

\section{Front Panel}

The front-panel has the following connections:
\begin{itemize}
\item Power supply interface for 220V AC
\item GEFE front interface including a VTRX optical fiber interface
\end{itemize}


\end{document}
