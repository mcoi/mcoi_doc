\NeedsTeXFormat{LaTeX2e}[1995/12/01]

\ProvidesClass{lhcreport}%
  [2009/05/17 v2.02 LaTeX document class]

\LoadClass[a4paper,12pt]{report}

% used packages. set page geometry. No options provided so far
\usepackage[left=25mm, right=25mm, top=3cm, bottom=4cm]{geometry}
\usepackage[T1]{fontenc}
\usepackage{lastpage}
\usepackage{tikz}
\usepackage{multicol}
\usepackage{multirow}
\usepackage{setspace}
%\usepackage{ltablex}
\usepackage[bookmarks=true]{hyperref}
\usepackage{tocloft}

% set various pdf and page lengths
\setlength\pdfvorigin{40mm}
\setlength\oddsidemargin{0pt}
\setlength\evensidemargin{0pt}
\setlength{\parindent}{0pt}

% serif font standard
\renewcommand{\familydefault}{\sfdefault}
\rmfamily

% various textx:
\renewcommand\contentsname{Table of Contents}

% TOC tweaks:
\renewcommand{\cfttoctitlefont}{\hfill\large\bfseries\itshape}
\renewcommand{\cftaftertoctitle}{\hfill}
\setlength{\cftbeforetoctitleskip}{-0.5cm}
\setlength{\cftaftertoctitleskip}{0cm}
\setlength{\cftbeforechapskip}{1mm}
\setlength{\cftbeforesecskip}{1mm}
\setlength{\cftbeforesubsecskip}{1mm}
\setlength{\cftbeforesubsubsecskip}{1mm}
\setlength{\cftsecindent}{0mm}
\setlength{\cftsubsecindent}{0mm}
\setlength{\cftsubsubsecindent}{0mm}
\renewcommand{\cftchapleader}{\bfseries\cftdotfill{\cftdotsep}}



% blue colorisation of active links
\hypersetup{colorlinks=true, linkcolor=blue,  anchorcolor=blue,
citecolor=blue, filecolor=blue, menucolor=blue,
urlcolor=blue}
% global redefinition of keywords:
\gdef\@keywords{No Keywords Given}%
\gdef\@title{No Title Given}%
\gdef\@author{No Author Given}%
\gdef\@docnumber{DOCNUMBER MISSING}
\gdef\@cernsec{CERNSEC MISSING}
\gdef\@edms{EDMS MISSING}
\gdef\@keywords{KEYWORDS MISSING}
\gdef\@doctype{Functional Specification}
\gdef\@abstract{NO ABSTRACT SO FAR}
\gdef\@approvalmembers{}
\gdef\@email{}
\gdef\@approvalleader{}
\gdef\@checkedby{}

\def\docnumber#1{\gdef\@docnumber{#1}}
\def\cernsec#1{\gdef\@cernsec{#1}}
\def\edms#1{\gdef\@edms{#1}}
\def\keywords#1{\gdef\@keywords{#1}}
\def\doctype#1{\gdef\@doctype{#1}}
\def\abstract#1{\gdef\@abstract{#1}}
\def\approvalmembers#1{\gdef\@approvalmembers{#1}}
\def\email#1{\gdef\@email{#1}}
\def\approvalleader#1{\gdef\@approvalleader{#1}}
\def\checkedby#1{\gdef\@checkedby{#1}}

% renew bibliography to contain the numbered chapter
\renewcommand\bibname{REFERENCES}
\renewenvironment{thebibliography}[1]
     {\chapter{\bibname}%
      \@mkboth{\MakeUppercase\bibname}{\MakeUppercase\bibname}%
      \list{\@biblabel{\@arabic\c@enumiv}}%
           {\settowidth\labelwidth{\@biblabel{#1}}%
            \leftmargin\labelwidth
            \advance\leftmargin\labelsep
            \@openbib@code
            \usecounter{enumiv}%
            \let\p@enumiv\@empty
            \renewcommand\theenumiv{\@arabic\c@enumiv}}%
      \sloppy
      \clubpenalty4000
      \@clubpenalty \clubpenalty
      \widowpenalty4000%
      \sfcode`\.\@m}

% tables of fixed width and defined centerings:
\newcommand{\PreserveBackslash}[1]{\let\temp=\\#1\let\\=\temp}
\let\PBS=\PreserveBackslash


\def\ps@plain{%
  \renewcommand*\@evenfoot{\@oddfoot}
  \renewcommand*\@evenhead{\@oddhead}
  \renewcommand*\@oddfoot{%
  \begin{tikzpicture}[remember picture, overlay]
    \node [shift={(-62mm,-2cm)}]  at (current page.north east)
    {%
      \begin{tikzpicture}[remember picture, overlay]
        % text centered
        \node [rectangle, draw,rounded corners=8pt, text centered, text width=90mm, line width=0.5mm](projnum)
        {%
          \tiny{LHC Project Document No.}\\
          \normalsize\textbf{\@docnumber}
        };
        \node[below of=projnum, text width=90mm]{
          \begin{flushright}
            Page \thepage~of~\pageref{LastPage}
          \end{flushright}
        };
      \end{tikzpicture}
    };
    \begin{tikzpicture}[remember picture, overlay]
      \node at (current page.south west)
      {%
        \draw [rounded corners=15pt,line width=0.3mm] (14mm, 20mm) rectangle +(180mm,241mm);
      };
    \end{tikzpicture}
  \end{tikzpicture}}
  \renewcommand*\@oddhead{\empty}}

% creator of the title page. Based on report document style!
\renewcommand\maketitle{%
  % PDFLatex style sheet. This must be issued before maketitle!
  \pdfinfo
  {
    /Title (\@title)
    /Creator (TeX)
    /Producer (pdfTex)
    /Author (\@author)
    /Subject (Example)
    /Keywords (\@keywords)
  }
  \begin{titlepage}%
    % drawing of lines and document numbers
    \begin{tikzpicture}[remember picture, overlay]
      \node [shift={(-58mm,-2cm)}]  at (current page.north east)
      {%
        \begin{tikzpicture}[remember picture, overlay, node distance = 11mm]
          % text centered
          \node [rectangle, draw,rounded corners=8pt, text centered, text width=90mm, line width=0.5mm](projnum)
          {%
            \tiny{LHC Project Document No.}\\
            \normalsize\textbf{\@docnumber}
          };
          \node [below of=projnum,rectangle, draw,rounded corners=8pt, text centered, text width=90mm, line width=0.3mm](cerndiv)
          {%
            \tiny{CERN Div./Group or Supplier/Contractor Document No.}\\
            \normalsize\textbf{\@cernsec}
          };
          \node [below of=cerndiv,rectangle, draw,rounded corners=8pt, text centered, text width=90mm, line width=0.3mm](edmsno)
          {%
            \tiny{EDMS Document No.}\\
            \normalsize\textbf{\@edms}
          };
          \node[below of=edmsno, text width=85mm, node distance=14mm]{
            \begin{flushright}
              Date: \@date
            \end{flushright}
          };
        \end{tikzpicture}
      };
      \begin{tikzpicture}[remember picture, overlay]
        \node at (current page.south west)
        {%
          \draw [rounded corners=15pt,line width=0.3mm] (14mm, 20mm) rectangle +(184mm,217mm);
        };
      \end{tikzpicture}
    \end{tikzpicture}
    % drawing CERN LHC logo and underlying text
    \begin{tikzpicture}[remember picture, overlay]
      \node [shift={(40mm,-36mm)}, rectangle, text width=50mm] at (current page.north west) (address)
      {%
        \begin{tabular*}{60mm}{ll}
          \Large\textbf{CERN}&\\
          \multicolumn{2}{l}{\large CH-1211 Geneva 23}\\
          \large Switzerland&\\
          \multirow{5}{*}{\noindent\includegraphics[width=2.7cm]{LHClogoBW}}
          &the\\
          &\textbf{Large}\\
          &\textbf{Hadron}\\
          &\textbf{Collider}\\
          &project\\
        \end{tabular*}
      };
    \end{tikzpicture}
    % drawing document namd, abstract
    \begin{tikzpicture}[remember picture, overlay]
      \node [shift={(99mm+8mm,225mm)}, text width=170mm, text centered ] at (current page.south west) (doctype)
      {%
        \Large\textbf{\@doctype}
      };
      \setstretch{1.5}%
      \node [below of=doctype, text width=170mm, text badly centered, node distance=30mm] (docname)
      {%
        \LARGE\textbf{\@title}
      };
      \setstretch{1}%
      \node [below of=docname, text width=170mm, text badly centered, node distance=25mm] (abstitle)
      {%
        \normalsize\textbf{\textit{\abstractname}}
      };
      \node [below of=abstitle, text width=170mm, text justified, anchor=north] (abstract){%
        \@abstract
      };
    \end{tikzpicture}
    \begin{tikzpicture}[remember picture, overlay]
      \node [shift={(100mm,30mm)}, text width=170mm, text centered, anchor=south] at (current page.south west) (checks)
      {%
        \begin{tabular*}{184mm}[t]{>{\PBS\centering}m{60mm}|>{\PBS\centering}p{65mm}|>{\PBS\centering}p{45mm}}
          \hline
          \textbf{\textit{Prepared by:}} & \textbf{\textit{Checked by:}} & \textbf{\textit{Approval Leader:}}\vspace{0.5mm}\\
          \begin{minipage}[t]{60mm}\centering\textbf{\@author}\\\href{mailto:\@email}{\@email}\end{minipage} & \begin{minipage}{60mm}\centering\textbf{\@checkedby}\end{minipage} & \textbf{\@approvalleader}\vfill\\
          \hline
          \multicolumn{3}{c}{\textbf{\textit{Approval Members:}}\vspace{1.5mm}}\\
          \multicolumn{3}{c}{%
            \begin{minipage}{155mm}
              \centering
              \@approvalmembers
            \end{minipage}}\\
        \end{tabular*}
      };
    \end{tikzpicture}
  \end{titlepage}%
  \setcounter{footnote}{0}%
  \global\let\thanks\relax
  \global\let\maketitle\relax
  \global\let\@thanks\@empty
  \global\let\@author\@empty
  \global\let\@date\@empty
  \global\let\@title\@empty
  \global\let\title\relax
  \global\let\author\relax
  \global\let\date\relax
  \global\let\and\relax
}
\renewcommand\chapter{\thispagestyle{plain}%
                    \global\@topnum\z@
                    \@afterindentfalse
                    \secdef\@chapter\@schapter}
\def\@chapter[#1]#2{\ifnum \c@secnumdepth >\m@ne
                         \refstepcounter{chapter}%
                         \typeout{\@chapapp\space\thechapter.}%
                         \addcontentsline{toc}{chapter}%
                                   {\protect\numberline{\thechapter}#1}%
                    \else
                      \addcontentsline{toc}{chapter}{#1}%
                    \fi
                    \chaptermark{#1}%
                    \addtocontents{lof}{\protect\addvspace{10\p@}}%
                    \addtocontents{lot}{\protect\addvspace{10\p@}}%
                      \@makechapterhead{#2}%
                      \@afterheading}
\def\@makechapterhead#1{%
  \vspace*{10\p@}%
  {\parindent \z@ \raggedright \normalfont
    \ifnum \c@secnumdepth >\m@ne
        \Large\bfseries \thechapter.\space
    \fi
    \interlinepenalty\@M
    \Large \bfseries \MakeUppercase{#1}\par\nobreak
    \vskip 10\p@
  }}
\def\@schapter#1{\@makeschapterhead{#1}%
                   \@afterheading}
\def\@makeschapterhead#1{%
  \vspace*{10\p@}%
  {\parindent \z@ \raggedright
    \normalfont
    \interlinepenalty\@M
    \Large \bfseries\MakeUppercase{#1}\par\nobreak
    \vskip 10\p@
  }}


% renew toc finished to keep table of contents on separate page
\renewcommand{\@cfttocstart}{%
  \if@cfthaschapter
    \if@twocolumn
      \@restonecoltrue\onecolumn
    \else
      \@restonecolfalse
    \fi
  \fi%
  \cleardoublepage}

\renewcommand{\@cfttocfinish}{%
  \if@cfthaschapter
    \if@restonecol\twocolumn\fi
  \fi%
  \cleardoublepage}


%%%%%%%%%%%%%%%%%%% IMPLEMENT NEW COUNTER - LIKE A TOC TABLE, HOWEVER IMPLEMENTS
% REGISTERS
\newcommand{\listmemoryname}{List of Assigned Memory Locations}
%\renewcommand{\cftaftertoctitle}{\hfill}
\newlistof{memory}{memlocs}{\listmemoryname}
\newcommand{\memory}[4]{%
\refstepcounter{memory}
\par\noindent\textbf{here we have to produce the thing \thememory. Arguments: #1 and #2 and #3}
\addcontentsline{memlocs}{memory}
{\protect\numberline\MakeUppercase #1 [#2,#3] \MakeUppercase #4}\par}

\renewcommand{\cftmemlocstitlefont}{\hfill\large\bfseries\itshape}
\renewcommand{\cftaftermemlocstitle}{\hfill}
\setlength{\cftbeforememlocstitleskip}{-0.5cm}
\setlength{\cftaftermemlocstitleskip}{2mm}


% new register environment, adds info into TOC
\newenvironment{register}
{\begin{enumerate}
  \setlength{\itemsep}{1pt}
  \setlength{\parskip}{0pt}
  \setlength{\parsep}{0pt}}
{\end{enumerate}}

\pagestyle{plain}
\endinput
%%
%%  End of file lhcreport.sty
